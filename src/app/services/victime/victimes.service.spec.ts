import { TestBed } from '@angular/core/testing';

import { VictimesService } from '../victime/victimes.service';

describe('VictimesService', () => {
  let service: VictimesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VictimesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
