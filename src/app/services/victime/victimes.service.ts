import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VictimesService {
  
  
  url:string = "https://jsonplaceholder.typicode.com/"; 

  $newVictime 

  constructor(private http:HttpClient) { }
  
  $allVictimes(){
      return this.http.get(`${this.url}users`);
  }

  $getVictime(id){
    return this.http.get(`${this.url}users/${id}`);
  }

  $addVictime(value){
    return this.http.post(this.url, value);
  }

  $updateVictime(id, value){
    return this.http.put(`${this.url}user/${id}`, value);
  }

  $deleteVictime(id){
    return this.http.delete(`${this.url}user/${id}`);
  }
}
