import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeclarationService {

  url:string = "https://jsonplaceholder.typicode.com/users"; 

  constructor(private http: HttpClient){}

  $allDeclarations(){
    return this.http.get(`${this.url}users`);
  }

  $getDeclaration(id){
    return this.http.get(`${this.url}users/${id}`);
  }

  $addDeclaration(value){
    return this.http.post(this.url, value);
  }

  $updateDeclaration(id, value){
    return this.http.put(`${this.url}user/${id}`, value);
  }

  $deleteDeclaration(id){
    return this.http.delete(`${this.url}user/${id}`);
  }
}
