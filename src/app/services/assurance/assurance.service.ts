import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AssuranceService {

  url:string = "https://jsonplaceholder.typicode.com/users"; 

  constructor(private http: HttpClient){}

  $allAssurances(){
    return this.http.get(`${this.url}users`);
  }

  $getAssurance(id){
    return this.http.get(`${this.url}users/${id}`);
  }

  $addAssurance(value){
    return this.http.post(this.url, value);
  }

  $updateAssurance(id, value){
    return this.http.put(`${this.url}user/${id}`, value);
  }

  $deleteAssurance(id){
    return this.http.delete(`${this.url}user/${id}`);
  }
}
