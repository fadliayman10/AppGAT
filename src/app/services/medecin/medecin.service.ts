import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MedecinService {

  url:string = "https://jsonplaceholder.typicode.com/users"; 

  constructor(private http: HttpClient){}

  $allMedecins(){
    return this.http.get(this.url);
  }  

  $getMedecin(id){
    return this.http.get(`${this.url}users/${id}`);
  }

  $addMedecin(value){
    return this.http.post(this.url, value);
  }

  $updateMedecin(id, value){
    return this.http.put(`${this.url}user/${id}`, value);
  }

  $deleteMedecin(id){
    return this.http.delete(`${this.url}user/${id}`);
  }
}
