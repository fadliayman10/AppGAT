import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CertificatMedicalService {

  url:string = "https://jsonplaceholder.typicode.com/users"; 

  constructor(private http: HttpClient){}

  $allCertificatMedical(){
    return this.http.get(`${this.url}users`);
  }

  $getCertificatMedical(id){
    return this.http.get(`${this.url}users/${id}`);
  }

  $addCertificatMedical(value){
    return this.http.post(this.url, value);
  }

  $updateCertificatMedical(id, value){
    return this.http.put(`${this.url}user/${id}`, value);
  }

  $deleteCertificatMedical(id){
    return this.http.delete(`${this.url}user/${id}`);
  }
}
