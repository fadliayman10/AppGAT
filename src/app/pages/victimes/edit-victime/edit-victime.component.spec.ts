import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVictimeComponent } from './edit-victime.component';

describe('EditVictimeComponent', () => {
  let component: EditVictimeComponent;
  let fixture: ComponentFixture<EditVictimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditVictimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVictimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
