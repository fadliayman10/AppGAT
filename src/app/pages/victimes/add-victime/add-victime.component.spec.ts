import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVictimeComponent } from './add-victime.component';

describe('AddVictimeComponent', () => {
  let component: AddVictimeComponent;
  let fixture: ComponentFixture<AddVictimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddVictimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVictimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
