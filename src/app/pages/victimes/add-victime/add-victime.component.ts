import { Component, OnInit } from '@angular/core';
import { Victimes } from 'src/app/interfaces/victimes';
import { VictimesService } from 'src/app/services/victime/victimes.service';

@Component({
  selector: 'app-add-victime',
  templateUrl: './add-victime.component.html',
  styleUrls: ['./add-victime.component.scss']
})
export class AddVictimeComponent implements OnInit {

  victime:Victimes

  constructor(private victimeService:VictimesService ) { }

  ngOnInit(): void {
  }

  onSubmit(value){
    // console.log(value);

    // this.victimeService.newVictime = value;

    this.victimeService.$addVictime(value).subscribe((result) =>{
      console.log(result);
      this.victimeService.$newVictime = result;
      
    })
    
  }
}
