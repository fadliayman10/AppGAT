import { Component, OnInit } from '@angular/core';
import { Victimes } from 'src/app/interfaces/victimes';
import { VictimesService } from 'src/app/services/victime/victimes.service';

@Component({
  selector: 'app-victime',
  templateUrl: './victime.component.html',
  styleUrls: ['./victime.component.scss']
})
export class VictimeComponent implements OnInit {

  victimes;
  formVictime:Victimes;

  constructor(private victimesServer: VictimesService) { }
  ngOnInit(): void {

    this.victimesServer.$allVictimes().subscribe((victime) => {
       this.victimes = victime;
    })
  }

  deleteVictime(value){
    console.log(value);
    this.victimesServer.$deleteVictime(value).subscribe(res => {
      console.log(res + "second console");
      
    })
    
  }

  // insertVictime(){
  //   this.victimesServer.addVictime().subscribe(res =>{
  //     console.log(res)
  //   })
  // }

  

  
}
