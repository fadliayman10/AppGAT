import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCertificatMedicalComponent } from './add-certificat-medical.component';

describe('AddCertificatMedicalComponent', () => {
  let component: AddCertificatMedicalComponent;
  let fixture: ComponentFixture<AddCertificatMedicalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCertificatMedicalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCertificatMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
