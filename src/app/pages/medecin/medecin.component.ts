import { Component, OnInit } from '@angular/core';
import { Medecin } from 'src/app/interfaces/medecin';
import { MedecinService } from 'src/app/services/medecin/medecin.service';

@Component({
  selector: 'app-medecin',
  templateUrl: './medecin.component.html',
  styleUrls: ['./medecin.component.scss']
})
export class MedecinComponent implements OnInit {

  medecins;

  constructor(private medecinService:MedecinService) { }

  ngOnInit(): void {
    this.medecinService.$allMedecins().subscribe(res => {
       this.medecins = res;
       console.log(res);
       
    })
  }

}
