import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { VictimeComponent } from './pages/victimes/victime/victime.component';
import { DeclarationComponent } from './pages/declaration/declaration.component';
import { CertificatMedicalComponent } from './pages/certificat-medical/certificat-medical.component';
import { UserComponent } from './pages/user/user.component';
import { MedecinComponent } from './pages/medecin/medecin.component';
import { AssuranceComponent } from './pages/assurance/assurance.component';
import { AddVictimeComponent } from './pages/victimes/add-victime/add-victime.component';
import { AddDeclarationComponent } from './pages/add-declaration/add-declaration.component';
import { AddCertificatMedicalComponent } from './pages/add-certificat-medical/add-certificat-medical.component';
import { AddMedecinComponent } from './pages/add-medecin/add-medecin.component';
import { AddAssuranceComponent } from './pages/add-assurance/add-assurance.component';
import { LoginComponent } from './pages/account/login/login.component';
import { RegisterComponent } from './pages/account/register/register.component';
import { EditVictimeComponent } from './pages/victimes/edit-victime/edit-victime.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ToastrModule.forRoot()
  ],
  declarations: [AppComponent, AdminLayoutComponent, AuthLayoutComponent, VictimeComponent, DeclarationComponent, CertificatMedicalComponent, UserComponent, MedecinComponent, AssuranceComponent, AddVictimeComponent, AddDeclarationComponent, AddCertificatMedicalComponent, AddMedecinComponent, AddAssuranceComponent, LoginComponent, RegisterComponent, EditVictimeComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
