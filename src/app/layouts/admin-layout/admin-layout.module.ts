import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { VictimeComponent } from "../../pages/victimes/victime/victime.component";
import { CertificatMedicalComponent } from "../../pages/certificat-medical/certificat-medical.component";
import { DeclarationComponent } from "../../pages/declaration/declaration.component";
import { UserComponent } from "../../pages/user/user.component";
import { MedecinComponent } from "../../pages/medecin/medecin.component";
import { AssuranceComponent } from "../../pages/assurance/assurance.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    VictimeComponent,
    CertificatMedicalComponent,
    DeclarationComponent,
    MedecinComponent,
    AssuranceComponent
  ]
})
export class AdminLayoutModule {}
