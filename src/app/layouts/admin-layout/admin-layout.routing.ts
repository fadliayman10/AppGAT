import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { VictimeComponent } from "../../pages/victimes/victime/victime.component";
import { CertificatMedicalComponent } from "../../pages/certificat-medical/certificat-medical.component";
import { DeclarationComponent } from "../../pages/declaration/declaration.component";
import { UserComponent } from "../../pages/user/user.component";
import { MedecinComponent } from "../../pages/medecin/medecin.component";
import { AssuranceComponent } from "../../pages/assurance/assurance.component";
import { AddVictimeComponent } from "../../pages/victimes/add-victime/add-victime.component";
import { AddDeclarationComponent } from "../../pages/add-declaration/add-declaration.component";
import { AddAssuranceComponent } from "../../pages/add-assurance/add-assurance.component";
import { AddCertificatMedicalComponent } from "../../pages/add-certificat-medical/add-certificat-medical.component";
import { AddMedecinComponent } from "../../pages/add-medecin/add-medecin.component";


export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "victime", component: VictimeComponent },
  { path: "add-victime", component: AddVictimeComponent },
  { path: "certificat-medical", component: CertificatMedicalComponent },
  { path: "add-certificat-medical", component: AddCertificatMedicalComponent },
  { path: "declaration", component: DeclarationComponent },
  { path: "add-declaration", component: AddDeclarationComponent },
  { path: "user", component: UserComponent },
  { path: "medecin", component: MedecinComponent },
  { path: "add-medecin", component: AddMedecinComponent },
  { path: "assurance", component: AssuranceComponent },
  { path: "add-assurance", component: AddAssuranceComponent }
];
