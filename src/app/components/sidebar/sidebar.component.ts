import { Component, OnInit } from "@angular/core";

declare interface RouteInfo {
  path: string;
  title: string;
  rtlTitle: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    rtlTitle: "لوحة القيادة",
    icon: "icon-chart-pie-36",
    class: ""
  },
  {
    path: "/victime",
    title: "Victime",
    rtlTitle: "الرموز",
    icon: "icon-badge",
    class: ""
  },
  {
    path: "/declaration",
    title: "Declaration",
    rtlTitle: "خرائط",
    icon: "icon-laptop",
    class: "" },
  {
    path: "/certificat-medical",
    title: "Certificat Médical",
    rtlTitle: "إخطارات",
    icon: "icon-notes",
    class: ""
  },

  {
    path: "/user",
    title: "Users",
    rtlTitle: "ملف تعريفي للمستخدم",
    icon: "icon-single-02",
    class: ""
  },
  {
    path: "/medecin",
    title: "Médecins",
    rtlTitle: "قائمة الجدول",
    icon: "icon-tie-bow",
    class: ""
  },
  {
    path: "/assurance",
    title: "Assurances",
    rtlTitle: "طباعة",
    icon: "icon-bank",
    class: ""
  },
  // {
  //   path: "/rtl",
  //   title: "RTL Support",
  //   rtlTitle: "ار تي ال",
  //   icon: "icon-world",
  //   class: ""
  // }
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
