import { Medecin } from './medecin';
import { Victimes } from './victimes';


export interface CertificatMedical {

    victime:Victimes;
    medecin:Medecin;
    type:string;
    nbrRepos:number;
    dateDebut: string;
    dateExpiration:string;
    description:string;


}
