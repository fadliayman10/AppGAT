import { Victimes } from "./victimes";

export interface Declaration {

    victime:Victimes;
    dateAcc:string;
    lieu:string;
    adresse:string;
    raison:string;
    degat:string;
    typeDegat:string;

}
