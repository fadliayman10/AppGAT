
export interface Victimes {
    nom:string;
    prenom:string;
    affiliationCNSS:string;
    sexe:string;
    addresse:string;
    profession:string;
    cin:string;
    nationalite:string;
    situationF:string;
    nbrEnfant:number;
    nbrConjoints:number;
    dateNaissance:string;
    dateRecrute:string;

}
