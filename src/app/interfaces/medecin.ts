export interface Medecin {

    nom:string;
    prenom:string;
    tel:string;
    cin:string;
    adresse:string;

}
